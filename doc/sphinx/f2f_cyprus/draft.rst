F2F in Cyprus draft presentations
=================================

This is WIP presentation intended to be presented at the Cyprus F2F.

EUABS all hands presentation
****************************
4IP project, what's inside ?:
    - Performances and energy metrics on PCP systems
    - UEABS on PCP machines
    - Porting to KNL + perf and energy analysis of the HORSE+MaPHyS+PaStiX stack

A project with tight deadlines
    - timeline
    - Wrapup machine access dates planned vs actual

UEABS on PCP machines:
    - plan code/machines BCOs
    MISSING check the fucking table !!!
PCP-GPU

PCP-KNL

PCP-FPGA

Session at ???
    - 1. deliverable quick talk and brainstorming
    - 2. current work overview with folling prez (cf attendants table)


EUABS session presentation
**************************

deliverable: comments/questions
    - 4IP-extension project intro
    - Machine specs
    - EUABS
        - intro: ref to previous D7.5
        - each code
            - what's new
            - few comments on perf/energy/scalability
        - wrap-up table
    - HORSE+MaPHyS+PaStiX stack
        - [...]
    - Conclusion

UEABS part:
   - results tables: comments questions
        - screen shot of the table
   - each code presentation
        - keep clear what's new from UEABS point of view, not because you're new to the code

Code presentations:
    - Specfem
    - les autres


EUABS Specfem3D_Globe presentation
***********************************

Quick prez
    - fortran
    - X lines of code
    - sismic code
Code version change on KNL
    - versioning is quite difficult
    - previously used intel modified version based on 7.0 version
    - Intel didn't released publicly the code, so it's still not officialy available
    - Decided to move back to the official version git rev = ??
KNL results
    - lost ~ x5 compared to the intel version which is pretty big
    - perf & energy table
    - WIP looking at atos results with UEABS test case
GPU
    - WIP compilation
    - no version change


UEABS Wrapup Session
********************

missing KNL runs: Specfem some further runs needed, Specfem3D_Globe  Code Saturn, riccard (ALYA), jacob (QCD), martti (GPAW), arno (CP2K & NEMO), Mariuscz (PFARM)
possibility to run on frioul (almost same environment, home filesystem shared, account activation form PCP one shoud be really easy)

more and updated informations/momtelcon on the gitlab website
next step is the deliverable -> worked on deadlines for contribution
Talked quiet a bit about that. Lots about figures whould be include and the way they are presented.

BCOs presentation vampirised the UEABS session
good presentation, thank you to all speakers. Please post your presentation to BSCW space:
thanks to every one that participated






EUABS session Mom
*****************

correct slide EUABS/UEABS

Deliverable
time & energy to solution + one column on "performance"
update which metric should be taken from energy tool on website
Showing total energy only is sad because PCP system have been thinked more featured. Whatever benchmarking is showing simple figures. hard to analyse metrics from multiple machine/energy stack.
walltime vs node consumption not good !! GFLOPs/energy max energy envelope
walltime not good because not visible -> use speedup normalize to 1.
update machine specs

Residency not inside the cost figure

QE:
 - not the 2nd UEABS testcase

Speedup vs cost pretty interresting plots


PFARM
metrics showed are for same testcase right ? just the number of node changing ? you should present speedup too

QCD
 KNL numbind -> cache mode very impacting.


Best Practise Guide session
===========================
Looking for volonteers
latest guides updates
