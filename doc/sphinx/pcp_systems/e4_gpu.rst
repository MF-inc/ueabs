Power8 + GPU
^^^^^^^^^^^^

D.A.V.I.D.E has been designed by `E4 computer engineering`_ and is hosted at CINECA_ in Bologna, Italy. It totals a theoretical peak performance of 990 TFlop/s (double precision). A more detailed description can be found on the `E4 dedicated webpage`_.

.. note:: In order to access the machine BCO should send an email to `Victor Cameo Ponz`_ so that.

Compute technology
""""""""""""""""""

Hardware features fat-nodes with the following design:
 * 45 nodes with

   * x2 IBM POWER8+ processors, ie 8x2 cores with Simultaneous Multi-Threading (SMT) 8
   * x4 NVIDIA P100 GPU with 16GB High Bandwidth Memory 2 (HBM2)
 * intranode communications integrated using NVLink
 * extranode communications integrated using Infiniband ERD interconnect in fat-tree with no oversubscription topology
 * CPU and GPU direct hot water (~27°C) cooling, removing 75-80% of the total heat
 * remaining 20-25% heat is air-cooled

Each compute node has a theoretical peak performance of 22 Tflop/s (double precision) and a power consumption of less than 2kW.

Energy sampling technology
""""""""""""""""""""""""""

Information is collected from processors, memory, GPUs and fans exploiting Analog-to-Digital Converter in the embedded SoC. It provides sampling up to 800 kHz lowered to 50kHz on power measuring sensor outputs.


The technology has been developed in collaboration with the University of Bologna which developed the :code:`get_job_energy <job_id>` program. Usage is straight forward and has the following verbose output:

.. literalinclude:: /pcp_systems/output_get_job_energy
   :emphasize-lines: 1


.. _E4 computer engineering: https://www.e4company.com
.. _E4 dedicated webpage: https://www.e4company.com/en/?id=press&section=1&page=&new=davide_supercomputer
.. _CINECA: http://hpc.cineca.it/
.. _Victor Cameo Ponz: cameo+4ip-extension@cines.fr

