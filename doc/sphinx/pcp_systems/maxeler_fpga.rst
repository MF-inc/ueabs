FPGA
^^^^

This machine has been designed by MAXELER_ and is hosted at JSC_ in Julich, Germany.

Compute technology
""""""""""""""""""

This small pilot system features:
 - 4 MPC-H servers including 2x MAX5 DFE and 2x Intel Xeon processors

Energy sampling technology
""""""""""""""""""""""""""


.. _MAXELER: http://maxeler.com/
.. _JSC: http://www.fz-juelich.de/ias/jsc/EN/Home/home_node.html
