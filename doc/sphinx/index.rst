.. UEABS for accelerators documentation master file, created by
   sphinx-quickstart on Wed Jun  7 19:01:00 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _4ip_toc:

Welcome to CINES documentation!
===============================

Organisation of the 4IP-extension is publicly available to anyone interrested in the project and rely on the following tools:
 * a slack channel for chat purpose: `PRACE PCP slack channel`_ , ask `Victor Cameo Ponz`_ for registration link.
 * a mailing list: prace-4ip-wp7.extension@fz-juelich.de, subscribe here: `mailing list registration page`_.
 * this documentation.

The following timeline will be followed to lead this task:
 #. white paper indicating what applications will be run on which prototypes (meeting the PRACE milestone **MS33 due by August 2017/M4**)
 #. grant access to machines (cut off September 2017 )
 #. run codes (cut off October/November)
 #. gather results and report *Applications Performance and Energy Usage* (this adresses the PRACE deliverable **D7.7 due by December 2017/M8**)


.. toctree::
   :maxdepth: 2
   :caption: General Table of Contents:
   :glob:

   ms33
   pcp_systems
   d77
   mom_telcon/index


.. _Victor Cameo Ponz: cameo+4ip-extension@cines.fr
.. _PRACE PCP slack channel: https://prace-pcp.slack.com
.. _mailing list registration page: https://lists.fz-juelich.de/mailman/listinfo/prace-4ip-wp7.extension


