During the PRACE-4IP extension, the main activity have been running the accelerated UEABS codes on the PRACE PCP prototypes to provide useful information on performance and energy usage for future Exascale systems. The project timeline have been divided in two main phases over six months:
 - Code preparation (2 months): investigating the PCP prototypes documentation and deciding what applications will be run and what prototypes will be involved. The list of applications and prototypes have been be published in the PRACE 4IP milestone MS33 available on the web[1] at the end of this phase.
 - Code run and analisys (4 months): measuring their performance, and monitoring their energy usage. These metrics and the corresponding analisys have been compiled in the PRACE 4IP delivereable D7.7. The final version of this document is currently under MB/TB approval phase.

In practice deadline setup for this project have been very tight. The major concern have been the availability of PCP systems, not only the openning date but also energy tools stack access and practice knowledge. In order to illustrate this fact, Figure 1 enlighthen the general timescale, while Table 1 gives precise dates for each PCP machine. Due to late access to the FPGA machine, this branch of execution has been abandonned so that the risk of losing time on it without result has been avoided. The extra time of 15 days for submitting the deliverable allowed us to run almost all envisoned benchmarks (see Figure 2 for more details).

Fig 1: timeline blabla

Table 1: PRACE 4IP extention access to PCP machine detailed dates

Here is the work done so far during the PRACE 4IP-extension:
 - Split off the UEABS git repository from the CodeVault repository
 - Compilation of MS33
 - Invesigation of the KNL and GPU prototype and their energy software stack
 - Run UEABS codes on above prototype (see details on Figure 2)
 - Portage and detailed performance and energy parametric study of the HORSE&MaPhys stack on KNL.
 - Compilation of the results in the D7.7

Fig 2: Code ran on PCP vs envisoned

Two major risks couldn't have been avoided during this project:
 - Machine availability: the FPGA schedule shifted to much to allow signigicant run. Unfortunately, hard deadlines doens't comply well with shcedule shift. Identify this type of risk early and try to plan "shortcuts" in the road map is the best we can do.
 - Running on prototypes is challenging by itself. Unexpected maintenances and tools stack availability didn't help with covering the road map. Having a clearer view on the PCP technical challenge would have help. However most of the roadmap have been covered so far


[1] PRACE 4IP MS33: https://misterfruits.gitlab.io/ueabs/ms33.html
