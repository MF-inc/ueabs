TelCon 15 of November
=====================

Apologies:
----------
 - Arno Proeme (EPCC)
 - Charles Moulinec (STFC)
 - Ricard Borrell (BSC)
 - Valeriu Codreanu (SurfSARA)
 - Volker Weinberg (LRZ)

Attending:
----------
 - Andrew Emerson (CINECA)
 - Dimitris Dellis (GRNET)
 - Jacob Finkenrath (CyI)
 - Mariusz Uchronski (WCNS/PSNC)
 - Martti Louhivuori (CSC)
 - Stéphane Lanteri (INRIA)
 - Victor Cameo Ponz (CINES)


Minutes of meeting:
-------------------


Deliverable update
******************

PMOs gave few extra days to submit the deliverable. It is now due by December the 13 instead of the 10. So partners will have few extra day to send me your contribution which is now due by **December the 3**.

.. note::  You can find table to be filled by BCO on this sestion: :ref:`deliverable_inputs`.

F2F Cyprus
**********

Meeting full day 23 of november, do not forget to register.
No more than 5 minutes talk presenting
Wether someone will present your code at in Cyprus and his name/contact

   +------------------------+-------------------------------+
   |                        |                               |
   |   Code name            |   Attending the F2F           +
   |                        |                               |
   +========================+===============================+
   | ALYA                   | Ricard Borrell (BSC)          |
   +------------------------+-------------------------------+
   | Code_Saturne           | ✗                             |
   +------------------------+-------------------------------+
   | CP2K                   | Alan Simpson (EPCC)           |
   +------------------------+-------------------------------+
   | GADGET                 | Volker Weinberg (LRZ)         |
   +------------------------+-------------------------------+
   | GPAW                   | Martti Louhivuori (CSC)       |
   +------------------------+-------------------------------+
   | GROMACS                | Dimitris Dellis (GRNET)       |
   +------------------------+-------------------------------+
   | NAMD                   | Dimitris Dellis (GRNET)       |
   +------------------------+-------------------------------+
   | NEMO                   | Alan Simpson (EPCC)           |
   +------------------------+-------------------------------+
   | PFARM                  | Mariusz Uchronski (WCNS/PSNC) |
   +------------------------+-------------------------------+
   | QCD                    | Jacob Finkenrath (CyI)        |
   +------------------------+-------------------------------+
   | Quantum Espresso       | Pietro Bonfa' (CINECA)        |
   +------------------------+-------------------------------+
   | SHOC                   | Damian Podareanu (SurfSARA)   |
   +------------------------+-------------------------------+
   | Specfem3D_Globe        | Victor Cameo Ponz (CINES)     |
   +------------------------+-------------------------------+

PCP-KNL news
************
Lots of you have asked for BEO access lately and sometimes it had been long to treat them. svp@cines.fr have been notified so they pay more attention to your demands.

Some issues in thread pinning. This can impact performances a lot.
Feedback needed for cache/flat mode on PCP ?

DAVIDE news
***********
The command to get some energy informations on DAVIDE is :code:`get_job_energy <job_id>`. It returns lots of informations and this is still uncertain of wich one exactly match our needs. However "IPMI Measures/Cumulative (all nodes)/Total energy" should the metric that we are looking for. Since don't have detailed documentation I wish you save the whole output of the command for the jobs so we can reprocess the result afterwards.

A. Emerson asked for his contact in Bologne to have more informations.

.. note:: Jobs ran one week ago should have energy metrics reccorded. However, before that the energy stack were not up so you should re-run your jobs.


PCP-FPGA news
*************
Machine has been opened. The 3 people that asked access, now have it.


Questions, concerns and report from BCOs
****************************************
Everything about login opening, deadlines, expected work during the next period, others:

   +------------------------+-----------------------------------------------------------+
   |                        |                                                           |
   |   Code name            +   4IP-extension BCO                                       +
   |                        |                                                           |
   +========================+===========================================================+
   | ALYA                   | starting tests with Alya on PCP KNL, code compiled and    |
   |                        | tests ready to run                                        |
   +------------------------+-----------------------------------------------------------+
   | Code_Saturne           | Installed on KNL using version 4.2.2. First test case     |
   |                        | (Taylor-Green vortex) has been successfully ran on 4      |
   |                        | nodes using 68 cores per node. Waiting for the support to |
   |                        | get BEO working. The second test (Code_Saturne with       |
   |                        | PETSc) will be carried out next on KNL and GPU.           |
   |                        | Priliminary results on both machines expectedby the F2F   |
   |                        | meeting                                                   |
   +------------------------+-----------------------------------------------------------+
   | CP2K                   | Some issues running one of the CP2K test cases on KNL,    |
   |                        | WIP. WIP also on GPU                                      |
   +------------------------+-----------------------------------------------------------+
   | GADGET                 | No feedback                                               |
   +------------------------+-----------------------------------------------------------+
   | GPAW                   | compiled run knl + got energy results                     |
   +------------------------+-----------------------------------------------------------+
   | GROMACS                | KNL, performances and energy metrics OK                   |
   +------------------------+ GPU performances OK, will have to rerun for energy        |
   | NAMD                   |                                                           |
   +------------------------+-----------------------------------------------------------+
   | NEMO                   | WIP                                                       |
   +------------------------+-----------------------------------------------------------+
   | PFARM                  | No more trouble with the GPU version                      |
   +------------------------+-----------------------------------------------------------+
   | QCD                    | Stuck on KNL with perf 20% -> look at process pinning     |
   |                        | GPU WIP, should work soon                                 |
   +------------------------+-----------------------------------------------------------+
   | Quantum Espresso       | KNL problems stoped with intel/intelmpi 18.0. MPI task    |
   |                        | pinning wird on PCP. Got results and energy. Will share   |
   |                        | pinning experience. GPU good results + energy.            |
   +------------------------+-----------------------------------------------------------+
   | SHOC                   | Access OK to KNL and GPU machines. Expect results for GPU |
   |                        | by F2F meeting. FPGA experiments end of november begining |
   |                        | of december                                               |
   +------------------------+-----------------------------------------------------------+
   | Specfem3D_Globe        | I've had tests cases running on KNL. I'm begining to look |
   |                        | at the GPU machine. Timing too short to run on FPGA       |
   +------------------------+-----------------------------------------------------------+

Date of next meeting
********************
 - 4 of december, 11:00 CET, primary PRACE number
