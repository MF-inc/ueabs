TelCon 2 of November
====================

Apologies:
----------
 - Arno Proeme (EPCC)
 - Charles Moulinec (STFC)
 - Jacob Finkenrath (CyI)
 - Mariusz Uchronski (WCNS/PSNC)

Present:
--------
 - Andrew Emerson (CINECA)
 - Hayk Shoukourian (LRZ)
 - Luigi Iapichino (LRZ)
 - Ricard Borrell (BSC)
 - Victor Cameo Ponz (CINES)
 - Volker Weinberg (LRZ)


Minutes of meeting:
-------------------

.. note:: The PRACE telcon line, ther service is very poor lately and a backup solution should be setup. This could be online base solution (Skype/Google hangout/some other online services). Anyone might have problem in getting access to microphone on an online connected machine ?

.. _deliverable_inputs:

General consideration about the deliverable
*******************************************

I'll need your inputs by the **30th of november**

 - **INRIA team**: you should include what have been discussed for the milestone
 - **UEABS BCO**: Few information about the code and comments about the scalability are required. You should fill at **leat one line of energy and performance metrics by machines by test case in this table**: :download:`OpenOffice<../deliverable_d7.7/deliverable_uabs_metrics.ods>` or :download:`Excel<../deliverable_d7.7/deliverable_uabs_metrics.xls>`

F2F Cyprus
**********

Meeting full day 23 of november, do not forget to register.
You will present the current state of the runs and results for the code you're in charge of during the 4IP/UEABS session. If not present, please ask one of your collegue to do the presentation for you. Ultimately I can present your work send me the presentation before the meeting.


PCP-KNL news
************
 - PSU full liquid cooled is being installed on half the machine today. That's the purpose of the hardware maintenance for the machine.
 - there will be another maintance beg/mid november that should stop 2 day the machine (probably around 13 of november).
 - cluster is and has always been in flat mode and might (or not...) move to cache mode. You will be notified if this happens. You can check the current state whith:

.. code-block:: shell

    sinfo -o "%30N %20b %f"

(can be added to job script to keep some trace)


DAVIDE news
***********
 - everyone supposed to run on this machine should have access now
 - Note the support contact: support@e4company.com
 - Andrew E. should be able to get the energy software stack documentation soon. It'll be spread as soon as possible.

PCP-FPGA news
*************
The few codes that requested access began the registration process. Still no clear opening date.


Questions, concerns and report from BCOs
****************************************
Everything about login opening, deadlines, expected work during the next period, others:

   +------------------------+-----------------------------------------------------------+
   |                        |                                                           |
   |   Code name            +   4IP-extension BCO                                       +
   |                        |                                                           |
   +========================+===========================================================+
   | ALYA                   | not started testing yet. test in a similar machine        |
   +------------------------+-----------------------------------------------------------+
   | Code_Saturne           | Out of office until the 7, not able to make progress on   |
   |                        |  code run                                                 |
   +------------------------+-----------------------------------------------------------+
   | CP2K                   | KNL: starting to get energy measurements, GPU built       |
   |                        | libraries and CP2K itself                                 |
   +------------------------+-----------------------------------------------------------+
   | GADGET                 |  uaebs code compiled ok. Initial condition pb with slurm. |
   |                        |  Ticket on CINES svp                                      |
   +------------------------+-----------------------------------------------------------+
   | GPAW                   |  No feedback                                              |
   +------------------------+-----------------------------------------------------------+
   | GROMACS                | KNL runs finished, all metrics gathered. Just have to     |
   +------------------------+ check scalability consistancy. GPU runs still WIP but     |
   | NAMD                   | Pregress lately. Exanged woth E4 support about that       |
   +------------------------+-----------------------------------------------------------+
   | NEMO                   | knl: built and ready for runs, GPU built netcdf & xios,   |
   |                        | starting to build NEMO                                    |
   +------------------------+-----------------------------------------------------------+
   | PFARM                  | Trouble finding the RMX_MAGMA_GPU source code. Asked      |
   |                        | Andrew Sunderland                                         |
   +------------------------+-----------------------------------------------------------+
   | QCD                    | successfully compiled QCD-part 2 on Davide, planning to   |
   |                        | have results before the F2F meeting                       |
   +------------------------+-----------------------------------------------------------+
   | Quantum Espresso       | Issues with KNL machine, intelMPI bug on bcast with F95.  |
   |                        | not exactly the same results as on Marconi (cache vs flat |
   |                        | ?). Starting running with GPU                             |
   +------------------------+-----------------------------------------------------------+
   | SHOC                   |  No feedback                                              |
   +------------------------+-----------------------------------------------------------+
   | Specfem3D_Globe        | No update since last teclon since I was out of office.    |
   |                        | I had new from atos that made great improve in running    |
   |                        | the code.                                                 |
   +------------------------+-----------------------------------------------------------+

AoB - Date of next meeting
****************************
 - 15 of November, 11:00 CEST, primary PRACE number
