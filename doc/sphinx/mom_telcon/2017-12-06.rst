TelCon 15 of November
=====================

Apologies:
----------
Attending:
----------



 - Arno Proeme (EPCC)
 - Charles Moulinec (STFC)
 - Ricard Borrell (BSC)
 - Valeriu Codreanu (SurfSARA)
 - Volker Weinberg (LRZ)
 - Andrew Emerson (CINECA)
 - Dimitris Dellis (GRNET)
 - Jacob Finkenrath (CyI)
 - Mariusz Uchronski (WCNS/PSNC)
 - Martti Louhivuori (CSC)
 - Stéphane Lanteri (INRIA)
 - Victor Cameo Ponz (CINES)


Minutes of meeting:
-------------------


Deliverable update
******************

I'm currently working on it. I'll let you access to the draft version by the end of the week.
I've had updated metrics as requested by for PFARM by Mariusz only.

PCP-KNL news
************
You can submit batch again, so feel free !


DAVIDE news
***********
Some trouble to get energy metrics at the end the last week.
Also no SMT activation available.
No feedback from support yet on my side

PCP-FPGA news
*************
No news
Any question ?

Questions, concerns and report from BCOs
****************************************
Everything about login opening, deadlines, expected work during the next period, others:

   +------------------------+-------+-------+-----------------------------------------------------------+
   |                        |   Results     |                                                           |
   |   Code name            +-------+-------+   4IP-extension BCO                                       +
   |                        |  KNL  |  GPU  |                                                           |
   +========================+=======+=======+===========================================================+
   | ALYA                   |       |       | starting tests with Alya on PCP KNL, code compiled and    |
   |                        |       |       | tests ready to run                                        |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | Code_Saturne           |       |       | Installed on KNL using version 4.2.2. First test case     |
   |                        |       |       | (Taylor-Green vortex) has been successfully ran on 4      |
   |                        |       |       | nodes using 68 cores per node. Waiting for the support to |
   |                        |       |       | get BEO working. The second test (Code_Saturne with       |
   |                        |       |       | PETSc) will be carried out next on KNL and GPU.           |
   |                        |       |       | Priliminary results on both machines expectedby the F2F   |
   |                        |       |       | meeting                                                   |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | CP2K                   |       |       | Some issues running one of the CP2K test cases on KNL,    |
   |                        |       |       | WIP. WIP also on GPU                                      |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | GADGET                 |       |       | No feedback                                               |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | GPAW                   |       |       | compiled run knl + got energy results                     |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | GROMACS                |       |       | KNL, performances and energy metrics OK                   |
   +------------------------+-------+-------+ GPU performances OK, will have to rerun for energy        |
   | NAMD                   |       |       |                                                           |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | NEMO                   |       |       | WIP                                                       |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | PFARM                  |       |       | No more trouble with the GPU version                      |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | QCD                    |       |       | Stuck on KNL with perf 20% -> look at process pinning     |
   |                        |       |       | GPU WIP, should work soon                                 |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | Quantum Espresso       |       |       | KNL problems stoped with intel/intelmpi 18.0. MPI task    |
   |                        |       |       | pinning wird on PCP. Got results and energy. Will share   |
   |                        |       |       | pinning experience. GPU good results + energy.            |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | SHOC                   |       |       | Access OK to KNL and GPU machines. Expect results for GPU |
   |                        |       |       | by F2F meeting. FPGA experiments end of november begining |
   |                        |       |       | of december                                               |
   +------------------------+-------+-------+-----------------------------------------------------------+
   | Specfem3D_Globe        |       |       | I've had tests cases running on KNL. I'm begining to look |
   |                        |       |       | at the GPU machine. Timing too short to run on FPGA       |
   +------------------------+-------+-------+-----------------------------------------------------------+

Date of next meeting
********************
 - 4 of december, 11:00 CET, primary PRACE number
