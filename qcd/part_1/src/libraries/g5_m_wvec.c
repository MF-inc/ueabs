/********************  g5_m_wvec.c  (in su3.a) ********************
*
*void g5_mult_wvec(wilson_vector *src, wilson_vector *dest) 
* Multiply a Wilson vector by gamma5 
* dest  <-  gamma5*src
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void g5_mult_wvec( wilson_vector * src, wilson_vector * dest )
{
    register int i;

    for ( i = 0; i < 3; i++ )
    {
	dest->COLORSPINOR( i, 0 ).real = src->COLORSPINOR( i, 0 ).real;
	dest->COLORSPINOR( i, 1 ).real = src->COLORSPINOR( i, 1 ).real;
	dest->COLORSPINOR( i, 2 ).real = -( src->COLORSPINOR( i, 2 ).real );
	dest->COLORSPINOR( i, 3 ).real = -( src->COLORSPINOR( i, 3 ).real );

	dest->COLORSPINOR( i, 0 ).imag = src->COLORSPINOR( i, 0 ).imag;
	dest->COLORSPINOR( i, 1 ).imag = src->COLORSPINOR( i, 1 ).imag;
	dest->COLORSPINOR( i, 2 ).imag = -( src->COLORSPINOR( i, 2 ).imag );
	dest->COLORSPINOR( i, 3 ).imag = -( src->COLORSPINOR( i, 3 ).imag );
    }
}
