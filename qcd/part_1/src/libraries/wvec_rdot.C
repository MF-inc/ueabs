/*****************  wvec_rdot.c  (in su3.a) ******************************
*									*
* double wvec_rdot( wilson_vector *a, wilson_vector *b )			*
* return real part of dot product of two wilson_vectors			*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"
#include "../include/macros.h"

__targetHost__ double wvec_rdot( wilson_vector * a, wilson_vector * b )
{
    register double ar, ai, br, bi, ss;

    ar = a->COLORSPINOR( 0, 0 ).real;
    ai = a->COLORSPINOR( 0, 0 ).imag;
    br = b->COLORSPINOR( 0, 0 ).real;
    bi = b->COLORSPINOR( 0, 0 ).imag;
    ss = ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 0 ).real;
    ai = a->COLORSPINOR( 1, 0 ).imag;
    br = b->COLORSPINOR( 1, 0 ).real;
    bi = b->COLORSPINOR( 1, 0 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 0 ).real;
    ai = a->COLORSPINOR( 2, 0 ).imag;
    br = b->COLORSPINOR( 2, 0 ).real;
    bi = b->COLORSPINOR( 2, 0 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 1 ).real;
    ai = a->COLORSPINOR( 0, 1 ).imag;
    br = b->COLORSPINOR( 0, 1 ).real;
    bi = b->COLORSPINOR( 0, 1 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 1 ).real;
    ai = a->COLORSPINOR( 1, 1 ).imag;
    br = b->COLORSPINOR( 1, 1 ).real;
    bi = b->COLORSPINOR( 1, 1 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 1 ).real;
    ai = a->COLORSPINOR( 2, 1 ).imag;
    br = b->COLORSPINOR( 2, 1 ).real;
    bi = b->COLORSPINOR( 2, 1 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 2 ).real;
    ai = a->COLORSPINOR( 0, 2 ).imag;
    br = b->COLORSPINOR( 0, 2 ).real;
    bi = b->COLORSPINOR( 0, 2 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 2 ).real;
    ai = a->COLORSPINOR( 1, 2 ).imag;
    br = b->COLORSPINOR( 1, 2 ).real;
    bi = b->COLORSPINOR( 1, 2 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 2 ).real;
    ai = a->COLORSPINOR( 2, 2 ).imag;
    br = b->COLORSPINOR( 2, 2 ).real;
    bi = b->COLORSPINOR( 2, 2 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 3 ).real;
    ai = a->COLORSPINOR( 0, 3 ).imag;
    br = b->COLORSPINOR( 0, 3 ).real;
    bi = b->COLORSPINOR( 0, 3 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 3 ).real;
    ai = a->COLORSPINOR( 1, 3 ).imag;
    br = b->COLORSPINOR( 1, 3 ).real;
    bi = b->COLORSPINOR( 1, 3 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 3 ).real;
    ai = a->COLORSPINOR( 2, 3 ).imag;
    br = b->COLORSPINOR( 2, 3 ).real;
    bi = b->COLORSPINOR( 2, 3 ).imag;
    ss += ar * br + ai * bi;

    return ( ss );
}



// version of the above ported to targetDP

extern __targetConst__ int t_sites_on_node;


__targetEntry__ void wvec_rdot_tdp_lattice(double* t_result, wilson_vector* t_cg_p,  wilson_vector* t_cg_mp){


  int i;

  __targetTLP__(i,t_sites_on_node)
    {
      
	wvec_rdot_tdp(&(t_result[i]),(double*) &( t_cg_p[0] ), i, (double*) &( t_cg_mp[0] ), i);
      
      
    }


}


__target__ double wvec_rdot_tdp( double* ss, double * a, int isitea, double * b, int isiteb )
{
  double ar[VVL], ai[VVL], br[VVL], bi[VVL];
  int iv=0;

    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,0,0,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,0,0,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,0,0,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,0,0,IMPART)];
    __targetILP__(iv) ss[iv] = ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,1,0,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,1,0,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,1,0,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,1,0,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,2,0,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,2,0,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,2,0,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,2,0,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];

    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,0,1,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,0,1,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,0,1,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,0,1,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,1,1,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,1,1,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,1,1,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,1,1,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,2,1,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,2,1,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,2,1,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,2,1,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];

    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,0,2,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,0,2,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,0,2,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,0,2,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,1,2,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,1,2,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,1,2,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,1,2,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,2,2,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,2,2,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,2,2,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,2,2,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];

    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,0,3,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,0,3,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,0,3,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,0,3,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,1,3,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,1,3,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,1,3,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,1,3,IMPART)];
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];
    __targetILP__(iv) ar[iv] = a[WVI(isitea+iv,2,3,REPART)];
    __targetILP__(iv) ai[iv] = a[WVI(isitea+iv,2,3,IMPART)];
    __targetILP__(iv) br[iv] = b[WVI(isiteb+iv,2,3,REPART)];
    __targetILP__(iv) bi[iv] = b[WVI(isiteb+iv,2,3,IMPART)];  
    __targetILP__(iv) ss[iv] += ar[iv] * br[iv] + ai[iv] * bi[iv];

    
}
