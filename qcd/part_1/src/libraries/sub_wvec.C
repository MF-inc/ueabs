/********************  sub_wvec.c  (in su3.a) ********************
*
*void sub_wilson_vector(wilson_vector *src1,*src2,*dest)
*  sub two Wilson vectors
* dest  <-  src1 + src2
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"


void sub_wilson_vector( wilson_vector * src1, wilson_vector * src2, wilson_vector * dest )
{
    dest->COLORSPINOR( 0, 0 ).real = src1->COLORSPINOR( 0, 0 ).real - src2->COLORSPINOR( 0, 0 ).real;
    dest->COLORSPINOR( 0, 0 ).imag = src1->COLORSPINOR( 0, 0 ).imag - src2->COLORSPINOR( 0, 0 ).imag;
    dest->COLORSPINOR( 1, 0 ).real = src1->COLORSPINOR( 1, 0 ).real - src2->COLORSPINOR( 1, 0 ).real;
    dest->COLORSPINOR( 1, 0 ).imag = src1->COLORSPINOR( 1, 0 ).imag - src2->COLORSPINOR( 1, 0 ).imag;
    dest->COLORSPINOR( 2, 0 ).real = src1->COLORSPINOR( 2, 0 ).real - src2->COLORSPINOR( 2, 0 ).real;
    dest->COLORSPINOR( 2, 0 ).imag = src1->COLORSPINOR( 2, 0 ).imag - src2->COLORSPINOR( 2, 0 ).imag;

    dest->COLORSPINOR( 0, 1 ).real = src1->COLORSPINOR( 0, 1 ).real - src2->COLORSPINOR( 0, 1 ).real;
    dest->COLORSPINOR( 0, 1 ).imag = src1->COLORSPINOR( 0, 1 ).imag - src2->COLORSPINOR( 0, 1 ).imag;
    dest->COLORSPINOR( 1, 1 ).real = src1->COLORSPINOR( 1, 1 ).real - src2->COLORSPINOR( 1, 1 ).real;
    dest->COLORSPINOR( 1, 1 ).imag = src1->COLORSPINOR( 1, 1 ).imag - src2->COLORSPINOR( 1, 1 ).imag;
    dest->COLORSPINOR( 2, 1 ).real = src1->COLORSPINOR( 2, 1 ).real - src2->COLORSPINOR( 2, 1 ).real;
    dest->COLORSPINOR( 2, 1 ).imag = src1->COLORSPINOR( 2, 1 ).imag - src2->COLORSPINOR( 2, 1 ).imag;

    dest->COLORSPINOR( 0, 2 ).real = src1->COLORSPINOR( 0, 2 ).real - src2->COLORSPINOR( 0, 2 ).real;
    dest->COLORSPINOR( 0, 2 ).imag = src1->COLORSPINOR( 0, 2 ).imag - src2->COLORSPINOR( 0, 2 ).imag;
    dest->COLORSPINOR( 1, 2 ).real = src1->COLORSPINOR( 1, 2 ).real - src2->COLORSPINOR( 1, 2 ).real;
    dest->COLORSPINOR( 1, 2 ).imag = src1->COLORSPINOR( 1, 2 ).imag - src2->COLORSPINOR( 1, 2 ).imag;
    dest->COLORSPINOR( 2, 2 ).real = src1->COLORSPINOR( 2, 2 ).real - src2->COLORSPINOR( 2, 2 ).real;
    dest->COLORSPINOR( 2, 2 ).imag = src1->COLORSPINOR( 2, 2 ).imag - src2->COLORSPINOR( 2, 2 ).imag;

    dest->COLORSPINOR( 0, 3 ).real = src1->COLORSPINOR( 0, 3 ).real - src2->COLORSPINOR( 0, 3 ).real;
    dest->COLORSPINOR( 0, 3 ).imag = src1->COLORSPINOR( 0, 3 ).imag - src2->COLORSPINOR( 0, 3 ).imag;
    dest->COLORSPINOR( 1, 3 ).real = src1->COLORSPINOR( 1, 3 ).real - src2->COLORSPINOR( 1, 3 ).real;
    dest->COLORSPINOR( 1, 3 ).imag = src1->COLORSPINOR( 1, 3 ).imag - src2->COLORSPINOR( 1, 3 ).imag;
    dest->COLORSPINOR( 2, 3 ).real = src1->COLORSPINOR( 2, 3 ).real - src2->COLORSPINOR( 2, 3 ).real;
    dest->COLORSPINOR( 2, 3 ).imag = src1->COLORSPINOR( 2, 3 ).imag - src2->COLORSPINOR( 2, 3 ).imag;
}


// version of the above ported to targetDP

extern __targetConst__ int t_sites_on_node;

__target__ void sub_wilson_vector_tdp( double * src1, double * src2, double * dest, int isite )
{

  int iv=0;

  __targetILP__(iv) dest[WVI(isite+iv,0,0,REPART)] = src1[WVI(isite+iv,0,0,REPART)] - src2[WVI(isite+iv,0,0,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,0,0,IMPART)] = src1[WVI(isite+iv,0,0,IMPART)] - src2[WVI(isite+iv,0,0,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,0,REPART)] = src1[WVI(isite+iv,1,0,REPART)] - src2[WVI(isite+iv,1,0,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,0,IMPART)] = src1[WVI(isite+iv,1,0,IMPART)] - src2[WVI(isite+iv,1,0,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,0,REPART)] = src1[WVI(isite+iv,2,0,REPART)] - src2[WVI(isite+iv,2,0,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,0,IMPART)] = src1[WVI(isite+iv,2,0,IMPART)] - src2[WVI(isite+iv,2,0,IMPART)];
  
  __targetILP__(iv) dest[WVI(isite+iv,0,1,REPART)] = src1[WVI(isite+iv,0,1,REPART)] - src2[WVI(isite+iv,0,1,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,0,1,IMPART)] = src1[WVI(isite+iv,0,1,IMPART)] - src2[WVI(isite+iv,0,1,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,1,REPART)] = src1[WVI(isite+iv,1,1,REPART)] - src2[WVI(isite+iv,1,1,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,1,IMPART)] = src1[WVI(isite+iv,1,1,IMPART)] - src2[WVI(isite+iv,1,1,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,1,REPART)] = src1[WVI(isite+iv,2,1,REPART)] - src2[WVI(isite+iv,2,1,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,1,IMPART)] = src1[WVI(isite+iv,2,1,IMPART)] - src2[WVI(isite+iv,2,1,IMPART)];
  
  __targetILP__(iv) dest[WVI(isite+iv,0,2,REPART)] = src1[WVI(isite+iv,0,2,REPART)] - src2[WVI(isite+iv,0,2,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,0,2,IMPART)] = src1[WVI(isite+iv,0,2,IMPART)] - src2[WVI(isite+iv,0,2,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,2,REPART)] = src1[WVI(isite+iv,1,2,REPART)] - src2[WVI(isite+iv,1,2,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,2,IMPART)] = src1[WVI(isite+iv,1,2,IMPART)] - src2[WVI(isite+iv,1,2,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,2,REPART)] = src1[WVI(isite+iv,2,2,REPART)] - src2[WVI(isite+iv,2,2,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,2,IMPART)] = src1[WVI(isite+iv,2,2,IMPART)] - src2[WVI(isite+iv,2,2,IMPART)];
  
  __targetILP__(iv) dest[WVI(isite+iv,0,3,REPART)] = src1[WVI(isite+iv,0,3,REPART)] - src2[WVI(isite+iv,0,3,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,0,3,IMPART)] = src1[WVI(isite+iv,0,3,IMPART)] - src2[WVI(isite+iv,0,3,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,3,REPART)] = src1[WVI(isite+iv,1,3,REPART)] - src2[WVI(isite+iv,1,3,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,1,3,IMPART)] = src1[WVI(isite+iv,1,3,IMPART)] - src2[WVI(isite+iv,1,3,IMPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,3,REPART)] = src1[WVI(isite+iv,2,3,REPART)] - src2[WVI(isite+iv,2,3,REPART)];
  __targetILP__(iv) dest[WVI(isite+iv,2,3,IMPART)] = src1[WVI(isite+iv,2,3,IMPART)] - src2[WVI(isite+iv,2,3,IMPART)];
}

__targetEntry__ void sub_wilson_vector_lattice( wilson_vector * src1, wilson_vector * src2, wilson_vector * dest){

  int i;
    __targetTLP__(i,t_sites_on_node)
    {

      sub_wilson_vector_tdp( (double*) &( src1[0] ), (double*) &( src2[0] ), (double*) &( dest[0] ), i );
    }


}
