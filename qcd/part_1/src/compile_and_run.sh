#!/bin/bash

export OMPI_CC=gcc-4.9

export OMP_NUM_THREADS=2

rm kernel_E.output bench
echo; echo; echo "Compiling:"; echo
#make clean; make -j 4 bench
make -j 4 bench

echo; echo; echo "Running:"; echo
./bench
#mpirun -np 2 ./bench

