#define CONTINUE 10
#define FRESH    11
#define RELOAD_ASCII  12
#define RELOAD_SERIAL  13
#define RELOAD_SERIAL_12  17
#define RELOAD_SERIAL_COMP  16
#define RELOAD_PARALLEL  14
#define LOAD_RANDOM 15
#define RELOAD_MULTIDUMP 18
#define FORGET 20
#define SAVE_ASCII 21
#define SAVE_SERIAL 22
#define SAVE_SERIAL_COMP 25
#define SAVE_PARALLEL 24
#define SAVE_CHECKPOINT 23
#define SAVE_MULTIDUMP 27
#define SAVE_SERIAL_ARCHIVE 30

/* Helps in defining globals */
#ifdef CONTROL
#define EXTERN
#else
#define EXTERN extern
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>		/* For "write" and "close" "off_t" */
#endif
#include <sys/types.h>		/* For "off_t" */
#include <stdio.h>
#include "../include/int32type.h"
#include "../include/macros.h"	/* For MAXFILENAME */


void reload_lattice( int flag, char *filename );
int ask_starting_lattice( char *latstart, int *flag, char *filename );
int get_f( FILE * f, char *variable_name_string, double * value );
int get_i_KE( FILE * f, char *variable_name_string, int *value );
int get_s_KE( FILE * f, char *variable_name_string, char *value );
int get_hbrho( FILE * f, char *variable_name_string );
int get_sw( FILE * f, char *variable_name_string );
int get_totnodes( FILE * f, char *variable_name_string );
