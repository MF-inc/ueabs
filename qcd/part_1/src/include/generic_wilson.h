#ifndef _GENERIC_WILSON_H
#define _GENERIC_WILSON_H
/************************ generic_wilson.h ******************************
 *									*
 *  Macros and declarations for generic_wilson routines                  *
 *  This header is for codes that call generic_wilson routines           *
 *  MIMD version 6 							*
 *									*
 */

#include "../include/su3.h"
#include "../include/macros.h"

/* matrix multiplications */
__targetHost__ void multiply_fmat( wilson_vector * src, wilson_vector * dest, int isign );
__targetHost__ void dslash( wilson_vector * src, wilson_vector * dest, int isign, int parity );

__targetHost__ void setup_dslash_comms();
__targetHost__ void finalise_dslash_comms();

__targetHost__ void latutil_cheb(double *tmp1, double *d, double *dd, double *qsrc, 
        double f1, double f2, double fch);


/* other */
__targetHost__ void unit_wvec(wilson_vector *wvec, int parity);
__targetHost__ void grand_wvec( wilson_vector * chi, int parity );
__targetHost__ void clear_latwvec( wilson_vector * chi, int parity );
__targetHost__ void funny_wvec( wilson_vector * chi );
__targetHost__ int setup_KE(  );
__targetHost__ int readin( int prompt );
__targetHost__ void meas_perf();

/* single precision stuff */
__targetHost__ void malloc_32bit();
__targetHost__ void free_32bit();
__targetHost__ void multiply_fmat_32( float * src, float * dest, int isign );
__targetHost__ void dslash_32( float *src, float *dest, int isign, int parity );
__targetHost__ void convert_gauge();
__targetHost__ void convert_wvec(wilson_vector *src, float *dest_32);
__targetHost__ void latutil_cheb_32(float *tmp1, float *d, float *dd, float *qsrc, 
        float f1, float f2, float fch);
__targetHost__ void latutil_xpay_32(float * x, float a, float * y, int parity);
__targetHost__ void latutil_axpy_32(float a, float * x, float * y, int parity);
__targetHost__ void latutil_5xpay_32(float * x, float a, float * y, int parity);
__targetHost__ double latutil_rdot_32(float * x, float * y, int parity);
__targetHost__ complex latutil_dot_32(float * x, float * y, int parity);
__targetHost__ double latutil_axpy_nrm2_32(float a, float * x, float * y, int parity);
__targetHost__ double latutil_nrm2_32(float * x, int parity);

#endif /* _GENERIC_WILSON_H */
