#CPU (CRAY XC30) configuration file

 CFLAGS = $(DEFINES) -O2 -DARCH=0 
 LDFLAGS = -lm -openmp 
 CC=cc
 TARGETCC=cc
 TARGETCFLAGS=-x c -openmp $(CFLAGS) -DVVL=4 -DAoSoA
