##############################################################################
#
#  Makefile.mk
#
#  Please copy one configuration file from the ./config
#  directory to this directory (top level Ludwig directory)
#  and make any appropriate changes for your platform.
#
#  No changes should be required in this file itself.
#
#
#  Edinburgh Soft Matter and Statistical Physics Group and
#  Edinburgh Parallel Computing Centre
#
#  (c) 2015 The University of Edinburgh
#  Contributing authors:
#  Kevin Stratford (kevin@epcc.ed.ac.uk)
#
##############################################################################

ROOT_DIR := $(dir $(lastword $(MAKEFILE_LIST)))


ifneq ("","$(wildcard $(ROOT_DIR)/config.mk)") #then file exists
include $(ROOT_DIR)/config.mk 
else
$(error $(ROOT_DIR)config.mk is missing: please copy one of the configuration files in the config directory to $(ROOT_DIR)config.mk)           
endif


