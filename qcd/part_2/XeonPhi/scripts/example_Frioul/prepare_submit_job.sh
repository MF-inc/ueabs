##
##
##
##
##

time=$1
n=$2
g=$3
perm=$4
scr=$5

sed 's/#NODES#/'${n}'/g' submit_job.sh.template > test
mv test submit_job.temp
sed 's/#TASKPERNODE#/'${g}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#TIME#/'${time}'/g' submit_job.temp > test
mv test $scr

if [ $perm -eq 1 ];then
	chmod +x $scr
fi
rm submit_job.temp
