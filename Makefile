.PHONY:doc clean

doc:
	$(MAKE) html -C doc/sphinx/

clean:
	$(MAKE) clean -C doc/sphinx/

